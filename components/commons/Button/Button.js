import { forwardRef } from "react";
import cn from "classnames";
import Link from "next/link";

import s from "./Button.module.css";
import { Icon } from "../";

const Button = forwardRef(
  (
    {
      type = "button",
      href,
      className,
      theme = 1,
      loading,
      reverse,
      disable,
      primary,
      large,
      circular,
      onClick = () => false,
      onMouseOver = () => false,
      icon,
      iconClassName,
      title,
      titleClassName,
      defaultColor,
      prefix = null,
      titleType = "LBodyR",
      iconPathCount = null,
      iconSize,
    },
    ref
  ) => {
    const Container = { link: "a", a: "a", button: "button" }[
      type === "link" && !href ? "a" : type
    ];
    const themeClass = `-theme-${theme}`;

    const _render = () => {
      return (
        <Container
          className={cn(s.container, className, {
            [s.primary]: primary,
            [s[themeClass]]: themeClass,
            [s.large]: large,
            [s.circular]: circular,
            [s.reverse]: reverse,
            [s.loading]: loading,
          })}
          ref={ref}
          onClick={(e) => {
            if (disable || loading) {
              e.preventDefault();
              return;
            }
            onClick(e);
          }}
          onMouseOver={(e) => {
            if (disable || loading) {
              e.preventDefault();
              return;
            }
            onMouseOver(e);
          }}
          href={href}
        >
          {prefix ? prefix : null}
          {icon && (
            <Icon
              icon={icon}
              iconSize={iconSize}
              className={iconClassName}
              iconPathCount={iconPathCount}
              style={{ color: defaultColor }}
            />
          )}
          {title && (
            <span
              className={cn(
                titleType,
                s.title,
                titleClassName,
                "capitalizeFirstLetterText"
              )}
              style={{ color: defaultColor }}
            >
              {title}
            </span>
          )}
        </Container>
      );
    };

    return type === "link" && href ? (
      <Link href={href}>{_render()}</Link>
    ) : (
      _render()
    );
  }
);

export { Button };
