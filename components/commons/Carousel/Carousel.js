import { Fragment, useState, useRef, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Pagination } from "swiper/core";
SwiperCore.use([Pagination]);

import cn from "classnames";

import s from "./Carousel.module.css";

const Carousel = ({
	slides = [],
	navigatorLeftClassName,
	navigatorRightClassName,
	renderSlide = () => null,
	className,
	swiperClassName,
	multi,
	fullPage,
	product,
	activeIndex = 0,
	onChangeSlide = () => false,
	hasPagination = false,
	hasNavigator,
	renderLastItem,
	cardNumber = 3,
	spaceBetween = 30,
	direction = "horizontal",
	hasDynamicBullets = false,
	bulletTheme = 1,
	slidesPerGroup = 1,
	hasDynamicSlide = false,
	centeredSlides = false,
	breakpoints = {},
}) => {
	const swiper = useRef(null);
	const stateRef = useRef({
		inChange: false,
		activeIndex: 0,
	});
	const [state, setState] = useState(stateRef.current);
	let customProps = {};
	const themeClassBullet = `-theme-${bulletTheme}`;

	const change = (data) => {
		stateRef.current = { ...stateRef.current, ...data };
		setState(stateRef.current);
	};

	useEffect(() => {
		if (swiper.current.autoplay) {
			if (state.inChange) {
				swiper.current.autoplay.stop();
			} else {
				swiper.current.autoplay.start();
			}
		}
	}, [state.inChange]);

	useEffect(() => {
		onChangeSlide(state.activeIndex);
	}, [state.activeIndex]);

	useEffect(() => {
		if (activeIndex !== state.activeIndex) swiper.current.slideTo(activeIndex);
	}, [activeIndex]);

	if (product) {
		customProps = {
			...customProps,
			autoplay: { delay: 4000 },
			spaceBetween,
			slidesPerView: cardNumber,
			breakpoints,
		};
	} else if (fullPage) {
		customProps = {
			...customProps,
			spaceBetween,
			centeredSlides,
			slidesPerView: 1,
			autoplay: { delay: 400066666666 },
		};
	} else if (multi) {
		customProps = {
			...customProps,
			spaceBetween,
			slidesPerGroup,
			centeredSlides,
			slidesPerView: cardNumber,
			autoplay: { delay: 120000 },
			breakpoints,
		};
	}

	const pagination = {
		clickable: true,
		dynamicBullets: hasDynamicBullets,
		bulletActiveClass: s.activeBullet,
		bulletClass: cn(s.bullet, {
			[s[themeClassBullet]]: themeClassBullet,
		}),
	};

	return (
		<div
			className={cn(s.wrapper, className, {
				[s.fullPage]: fullPage,
				[s.multi]: multi,
				[s.inChange]: state.inChange,
				[s.product]: product,
				[s[direction]]: direction,
				[s.dynamicSlide]: hasDynamicSlide,
			})}
		>
			<Swiper
				{...customProps}
				pagination={hasPagination && pagination}
				direction={direction}
				onSwiper={(e) => (swiper.current = e)}
				className={cn(s.container, swiperClassName)}
				speed={500}
				onTransitionEnd={() => change({ inChange: false })}
				onTransitionStart={() => change({ inChange: true })}
				onSliderFirstMove={() => change({ inChange: true })}
				onActiveIndexChange={({ activeIndex }) => change({ activeIndex })}
			>
				{slides.map((item, index) => (
					<SwiperSlide key={item.id || index} className={s.slide}>
						{renderSlide(item, index)}
					</SwiperSlide>
				))}
				{renderLastItem && (
					<SwiperSlide className={s.slide}>{renderLastItem()}</SwiperSlide>
				)}
			</Swiper>
			{hasNavigator && (
				<Fragment>
					<button
						className={cn(s.sliderNavigator, navigatorRightClassName, s.prev, {
							[s.disabled]: state.activeIndex === 0,
						})}
						onClick={() => swiper.current.slidePrev()}
						onMouseEnter={() => (swiper.current.allowTouchMove = false)}
						onMouseLeave={() => (swiper.current.allowTouchMove = true)}
					>
						<i className={multi ? "icon-arrow-right" : "icon-arrow-right-2"} />
					</button>

					<button
						className={cn(s.sliderNavigator, navigatorLeftClassName, s.next, {
							[s.disabled]: slides.length - 1 === state.activeIndex,
						})}
						onClick={() => swiper.current.slideNext()}
						onMouseEnter={() => (swiper.current.allowTouchMove = false)}
						onMouseLeave={() => (swiper.current.allowTouchMove = true)}
					>
						<i className={multi ? "icon-arrow-right" : "icon-arrow-right-2"} />
					</button>
				</Fragment>
			)}
		</div>
	);
};

export { Carousel };
