import cn from "classnames";

import s from "./Icon.module.css";

const Icon = ({ icon, iconPathCount, iconSize, style, className }) => {
  return (
    <i
      className={cn(s.container, `icon-${icon}`, className)}
      style={{ ...style, "--iconSize": iconSize }}
    >
      {iconPathCount &&
        Array(Number(iconPathCount))
          .fill("_")
          .map((_, index) => <i className={`path${index + 1}`} key={index} />)}
    </i>
  );
};

export { Icon };
