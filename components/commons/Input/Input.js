import { useEffect, useState } from "react";
import cn from "classnames";

import s from "./Input.module.css";
import { utils } from "../../../utils";

const Input = ({
	placeholder,
	onChange = () => false,
	className,
	type,
	disabled,
	maxLength,
	onKeyPress = () => false,
	defaultValue = "",
	autoDirection = true,
	prefix = null,
	suffix = null,
	editable = false,
	onFocuseActive = () => false,
	wrapperClassName,
}) => {
	const [value, setValue] = useState(defaultValue);
	const [isActive, setIsActive] = useState(false);
	const isLtr = autoDirection;

	useEffect(() => {
		setValue(defaultValue);
	}, [defaultValue]);

	const handleChangeValue = (value) => {
		let val = value;
		val = val === null ? "" : String(val);
		val = type === "numberWithComma" ? val.split("،").join("") : val;
		val = utils.toEnglishDigits(val);
		if (
			(type === "numberWithComma" || type === "number") &&
			isNaN(Number(val))
		) {
			return;
		}
		onChange && onChange(val);
		setValue(val);
	};

	const printValue = () => {
		let val = value;
		val = type === "numberWithComma" ? utils.putCommas(val) : val;
		return val;
	};

	const props = {
		disabled,
		maxLength,
		onKeyPress,
		placeholder,
		value: printValue(),
		onChange: ({ target: { value } }) => handleChangeValue(value),
		className: s.input,
		onFocus: () => {
			setIsActive(true), onFocuseActive(true);
		},
		onBlur: () => {
			setIsActive(false), onFocuseActive(false);
		},
	};

	switch (type) {
		default:
		case "textarea":
		case "text":
			break;
		case "numberWithComma":
		case "number":
			props.inputMode = "numeric";
			props.pattern = "[0-9]*";
			break;
	}

	return (
		<div
			className={cn(s.container, className, {
				[s.textArea]: type === "textarea",
				[s.ltr]: isLtr && Boolean(value),
				[s.hasValue]: Boolean(value),
				[s.disabled]: disabled,
				[s.active]: isActive,
			})}
		>
			{prefix}
			<div className={cn(s.wrapper, wrapperClassName)}>
				{type === "textarea" ? (
					<textarea {...props} />
				) : (
					<input {...props} type="text" />
				)}
			</div>
			{editable && !Boolean(value) ? "" : suffix}
		</div>
	);
};

export { Input };
