import { useRef, useEffect } from "react";
import { Reoverlay } from "reoverlay";
import cn from "classnames";
import { gsap } from "gsap";

import s from "./ModalWrapper.module.css";
import { utils } from "../../../utils";
import { Button } from "..";

const ModalWrapper = ({
  children,
  className,
  hideClose,
  containerClassName,
  closeButtonType = 1,
}) => {
  const modal = useRef(null);
  const closeButton = {
    1: {
      title: "back",
      icon: "arrow-left",
    },
    2: {
      title: "close",
      icon: "close",
    },
  };

  useEffect(() => {
    utils.lockScroll();
    gsap.to(modal.current, {
      opacity: 1,
      duration: 0.5,
      ease: "Power1.easeOut",
    });
    return () => {
      utils.enableScroll();
    };
  }, []);

  const handleClose = () => {
    gsap.to(modal.current, {
      opacity: 0,
      duration: 0.3,
      ease: "Power1.easeOut",
      onComplete: () => Reoverlay.hideModal(),
    });
  };

  return (
    <div className={cn(s.container, containerClassName)} ref={modal}>
      {!hideClose && (
        <div className="container">
          <Button
            onClick={handleClose}
            className={s.closeButton}
            {...closeButton[closeButtonType]}
          />
        </div>
      )}
      <div className={cn(s.content, className)}>{children}</div>
    </div>
  );
};

export { ModalWrapper };
