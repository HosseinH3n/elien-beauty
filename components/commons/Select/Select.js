import { useState, useEffect, useRef } from "react";
import cn from "classnames";

import s from "./Select.module.css";

const Select = ({
	className,
	defaultSelected,
	options = [],
	placeholder = "choose ...",
	onChange = () => false,
	wrapperClassName,
	menuClassName,
}) => {
	const [selected, setSelected] = useState(defaultSelected);
	const [open, setOpen] = useState(false);
	const [size, setSize] = useState(0);
	const contentWrapper = useRef(null);
	const selectedWrapper = useRef(null);
	const selectedOption = options.find((item) => item.value === selected);

	useEffect(() => {
		onChange(selectedOption);
	}, [selected]);

	useEffect(() => {
		open && window.addEventListener("click", handleToggle);
		return () => {
			open && window.removeEventListener("click", handleToggle);
		};
	}, [open]);

	const handleToggle = () => {
		setSize(open ? 0 : contentWrapper.current.offsetHeight);
		setOpen(!open);
	};

	return (
		<div
			className={cn("flex-row", "justify-between", s.container, className)}
			onClick={handleToggle}
		>
			<div
				className={cn(s.selectedWrapper, wrapperClassName, "justify-between")}
				ref={selectedWrapper}
			>
				<p>{selectedOption ? selectedOption.label : placeholder}</p>
				<i className={cn("icon-arrow-down-2", s.icon)} />
			</div>
			<div
				className={cn(s.menu, menuClassName, {
					[s.open]: open,
				})}
				style={{
					height: `calc(100% + ${size}px)`,
				}}
			>
				<ul className={s.options} ref={contentWrapper}>
					{options.map(({ value, label }) => (
						<li
							key={value}
							className={cn({
								[s.active]: selected === value,
							})}
							onClick={() => setSelected(value)}
						>
							<span>{label}</span>
						</li>
					))}
				</ul>
			</div>
		</div>
	);
};

export { Select };
