import cn from "classnames";

import s from "./Border.module.css";

const Border = ({
  rotate = 10,
  offset_1 = 0,
  offset_2 = 0.35,
  offset_3 = 0.59,
  offset_4 = 1,
  className,
}) => {
  return (
    <svg className={cn(s.container, className)}>
      <defs>
        <linearGradient
          id="gradient"
          gradientUnits="objectBoundingBox"
          gradientTransform={`rotate(${rotate})`}
        >
          <stop offset={offset_1} style={{ stopColor: "#fff" }} />
          <stop
            offset={offset_2}
            style={{ stopColor: "#fff", stopOpacity: "0.24" }}
          />
          <stop
            offset={offset_3}
            style={{ stopColor: "#fff", stopOpacity: "0" }}
          />
          <stop offset={offset_4} style={{ stopColor: "#fff" }} />
        </linearGradient>
      </defs>
      <rect
        id="rect"
        fill="none"
        width="100%"
        height="100%"
        strokeWidth="1"
        className={s.rect}
        strokeMiterlimit="1"
        stroke="url(#gradient)"
      />
    </svg>
  );
};

export { Border };
