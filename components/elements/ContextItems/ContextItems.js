import { useState } from "react";
import cn from "classnames";

import s from "./ContextItems.module.css";
import { Button } from "../../commons";

const ContextItems = ({ items = [], href, className, itemClassName }) => {
  const [active, setActive] = useState(items[0]?.id);

  return (
    <>
      <div className={cn(s.container, className)}>
        {items.length > 0 &&
          items.map(({ id, title }) => (
            <Button
              key={id}
              title={title}
              className={cn(s.button, itemClassName, {
                [s.active]: id === active,
              })}
              onClick={() => setActive(id)}
            />
          ))}
      </div>
      <Button
        primary
        reverse
        href={
          items.length ? items.find((item) => item.id === active).href : href
        }
        type="link"
        icon="arrow-right"
        title="more details"
        className={s.moreButton}
      />
    </>
  );
};

export { ContextItems };
