import { Fragment, useEffect } from "react";
import cn from "classnames";
import gsap from "gsap";

import s from "./CurveStepper.module.css";
import { useUpdate } from "../../../hooks";

const CurveStepper = ({
  className,
  steps = [],
  selected,
  onSelect = () => false,
}) => {
  const active = steps.indexOf(steps.find((item) => item.id === selected));

  useEffect(() => {
    if (steps.length && selected) {
      clear();
    }
  }, [steps.length]);

  useUpdate(() => {
    start();
  }, [active]);

  const clear = (withStart = true) => {
    const easing = {
      duration: withStart ? 0.3 : 0.5,
      ease: "power3.in",
    };
    gsap.to(`.${s.line} svg path`, {
      attr: {
        d: "M 28 1 l 0 100 q 0 53 0 53",
      },
      onComplete: () => {
        gsap.set(`.${s.line}`, {
          scaleY: 1,
        });
        if (withStart) start();
      },
      ...easing,
    });
    gsap.to(`.${s.item}`, {
      x: 0,
      ...easing,
    });
    gsap.to(`.${s.title}`, {
      opacity: 0.4,
      ...easing,
    });
    gsap.to(`.${s.pointInner}`, {
      scale: 0.8,
      opacity: 0.1,
      ...easing,
    });
  };

  const start = () => {
    const easing = {
      duration: 0.5,
      ease: "power3.out",
    };
    gsap.set(`.${s.line}`, {
      scaleY: (n) => (active - n === 0 ? -1 : 1),
    });
    gsap.to(`.${s.line} svg path`, {
      attr: {
        d: (n) => {
          if (active - n === 1 || active - n === 0) {
            return "M 28 1 l 0 100 q -1 35 -23 53";
          }
          return "M 28 1 l 0 100 q 0 53 0 53";
        },
      },
      ...easing,
    });
    gsap.to(`.${s.item}`, {
      x: (n) => (n == active ? -33 : 0),
      ...easing,
    });
    gsap.to(`.${s.title}`, {
      opacity: (n) => (n == active ? 1 : 0.4),
      duration: 0.4,
      ease: "power1.out",
    });
    gsap.to(`.${s.pointInner}`, {
      scale: (n) => (n == active ? 0.5 : 0.8),
      opacity: (n) => (n == active ? 0.9 : 0.1),
      duration: 0.4,
      ease: "power2.out",
    });
  };

  if (!steps.length) return null;
  return (
    <div className={cn(s.container, className)}>
      {steps.map((item, index) => (
        <Fragment key={item.id}>
          <div
            className={s.item}
            onClick={() => {
              clear(false);
              onSelect(item);
            }}
          >
            <span className={s.title}>{item.title}</span>
            <div className={s.point}>
              <span className={s.pointInner}></span>
            </div>
          </div>
          {index === steps.length - 1 ? null : (
            <div className={s.line}>
              <svg viewBox="0 0 30 155" fill="none">
                <path
                  d="M 28 1 l 0 100 q 0 53 0 53"
                  stroke="#FFF"
                  fill="none"
                  strokeWidth={1.5}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeOpacity={0.3}
                />
              </svg>
            </div>
          )}
        </Fragment>
      ))}
    </div>
  );
};

export { CurveStepper };
