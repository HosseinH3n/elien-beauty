import { Fragment } from "react";
import cn from "classnames";

import s from "./Footer.module.css";
import { Button } from "../../commons";
import { GLOBAL_CONST } from "../../../constants";

const Footer = () => {
  const IMAGESCONFIG = [
    {
      id: 1,
      top: "110px",
      left: "0",
      img: "/media/static/24.png",
    },
    {
      id: 2,
      top: "80px",
      left: "calc(100% - 121.7px)",
      img: "/media/static/25.png",
    },
  ];
  const { description, copyright, links } = GLOBAL_CONST.footer;
  const iconPathCount = {
    calling: 3,
    message: 2,
    location: 2,
  };

  const _renderFloatImgs = (...slice) =>
    IMAGESCONFIG.slice(...slice).map(({ id, img, ...style }) => (
      <img key={id} src={img} style={style} className={s.floatImg} />
    ));

  const _renderSocialMedia = ({ title, items }) => (
    <Fragment>
      <span className={cn(s.socialMediaTitle, "MBodyR")}>{title}</span>
      <div className={s.socialMedia}>
        {items.map(({ id, ...meida }) => (
          <Button
            key={id}
            circular
            type="link"
            {...meida}
            className={s.socialMediaItems}
          />
        ))}
      </div>
    </Fragment>
  );

  return (
    <footer className={s.container}>
      {_renderFloatImgs(0, 1)}
      <div className={cn(s.wrapper, "container")}>
        <div className={s.context}>
          <img className={s.logo} src="/media/static/logo.svg" alt="logo" />
          <p className={cn(s.description, "MBodyR")}>{description}</p>
        </div>
        <small className={s.copyright}>{copyright}</small>
        <ul className={s.links}>
          {links.map(({ id, title, child, socialMedia }) => (
            <li key={id}>
              <h3 className={cn(s.titleLinks, "STitleB")}>{title}</h3>
              {child.length > 0 && (
                <Fragment>
                  <ul>
                    {child.map(({ id, ...items }) => (
                      <Button
                        key={id}
                        {...items}
                        titleType="MBodyR"
                        className={s.linksItem}
                        iconPathCount={iconPathCount[items.icon]}
                      />
                    ))}
                  </ul>
                  {socialMedia && _renderSocialMedia(socialMedia)}
                </Fragment>
              )}
            </li>
          ))}
        </ul>
        {_renderFloatImgs(1)}
      </div>
    </footer>
  );
};

export { Footer };
