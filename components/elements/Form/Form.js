import cn from "classnames";
import { toast } from "react-toastify";

import s from "./Form.module.css";
import { Input, Button } from "../../commons";
import { GLOBAL_CONST } from "../../../constants";
import { Border } from "..";

const Form = () => {
  const { formInputs } = GLOBAL_CONST;

  return (
    <div className={s.form}>
      {formInputs.map(({ label, ...items }) => (
        <Input
          key={label}
          className={s.input}
          {...{ ...items, label }}
          prefix={<span className={s.inputTitle}>{label}</span>}
          wrapperClassName={cn(s.inputWrapper, "backdropFilter")}
        />
      ))}
      <Button
        primary
        reverse
        theme="2"
        title="Verzenden"
        icon="arrow-right"
        prefix={<Border className={s.border} />}
        className={cn(s.submitButton, "backdropFilter")}
        onClick={() => toast("Success message !", { type: "success" })}
      />
    </div>
  );
};

export { Form };
