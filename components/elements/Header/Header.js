import cn from "classnames";
import { Reoverlay } from "reoverlay";
import Link from "next/link";

import s from "./Header.module.css";
import { Button } from "../../commons";
import { Border } from "../";
import { useMobile } from "../../../hooks";

const Header = () => {
  const isTablet = useMobile(1024);
  const gradient = isTablet && {
    offset_2: 0.4,
    offset_3: 0.5,
    rotate: 50,
  };

  return (
    <header className={cn(s.container, "container")}>
      <Button
        icon="menu"
        title="menu"
        circular={isTablet}
        titleClassName={s.buttonTitle}
        prefix={<Border className={s.border} {...gradient} />}
        onClick={() => Reoverlay.showModal("MenuModal")}
        className={cn(s.menuButton, s.button, "backdropFilter")}
      />
      <Link href="/">
        <a>
          <img
            className={s.logo}
            src="/media/static/logo.svg"
            alt="Elien Smekens"
          />
        </a>
      </Link>

      <Button
        icon="phone"
        circular={isTablet}
        title="get in touch"
        titleClassName={s.buttonTitle}
        prefix={<Border className={s.border} {...gradient} />}
        className={cn(s.button, "backdropFilter")}
        onClick={() => Reoverlay.showModal("ContactUsModal")}
      />
    </header>
  );
};

export { Header };
