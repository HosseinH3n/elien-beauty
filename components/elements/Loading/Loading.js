import React, { useEffect } from "react";
import gsap from "gsap";

import s from "./Loading.module.css";

const Loading = ({ onLoad }) => {
  useEffect(() => {
    function loaded() {
      gsap.to(`.${s.container}`, {
        opacity: 0,
        duration: 0.8,
        delay: 0.2,
        ease: "power1.in",
        onComplete: () => {
          onLoad();
          gsap.set(`.${s.container}`, {
            zIndex: -1,
          });
        },
      });
    }
    if (
      document.readyState === "complete" ||
      document.readyState === "interactive"
    ) {
      loaded();
    } else {
      window.onload = () => {
        loaded();
      };
    }
  }, []);

  return <div className={s.container}></div>;
};

export { Loading };
