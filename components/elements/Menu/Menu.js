import cn from "classnames";
import { useRouter } from "next/router";
import { Reoverlay } from "reoverlay";

import s from "./Menu.module.css";
import { HEADER_CONST } from "../../../constants";
import { Button, Icon } from "../../commons";

const Menu = () => {
  const { links, contactItems, socialMedia } = HEADER_CONST;
  const { asPath } = useRouter();

  return (
    <div className={cn(s.container, "container")}>
      <img
        className={s.logo}
        alt="Elien Smekens"
        src="/media/static/logo-2.svg"
      />
      <div className={s.links}>
        {links.map(({ id, href, ...items }) => (
          <Button
            type="link"
            key={id}
            {...items}
            href={href}
            onClick={() => Reoverlay.hideModal()}
            className={s.linksItem}
            titleClassName={cn(s.linksItemTitle, {
              [s.active]: href === asPath,
            })}
          />
        ))}
      </div>
      <div className={s.context}>
        <div>
          <img
            className={s.logo}
            alt="Elien Smekens"
            src="/media/static/logo-2.svg"
          />
          <h3 className={cn(s.title, "capitalizeText")}>elien smekens</h3>
          <div className={s.contact}>
            {contactItems.map(({ id, icon, caption, value }) => (
              <div key={id} className={s.contactItems}>
                <Icon icon={icon} iconPathCount={icon === "calling" ? 3 : 2} />
                <div>
                  <span className={s.contactItemsCpation}>{caption}</span>
                  <span>{value}</span>
                </div>
              </div>
            ))}
          </div>
          <div className={s.socialMedia}>
            {socialMedia.map(({ id, ...items }) => (
              <Button
                circular
                key={id}
                {...items}
                className={s.socialMediaItems}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export { Menu };
