import cn from "classnames";

import s from "./NextSection.module.css";
import { Icon } from "../../commons";

const NextSection = ({ title, position = {} }) => {
  return (
    <div className={s.container} style={position}>
      <Icon icon="mouse" />
      <span className={cn("capitalizeFirstLetterText", "XLBodyR")}>
        {title}
      </span>
      <Icon icon="arrow-down" />
    </div>
  );
};

export { NextSection };
