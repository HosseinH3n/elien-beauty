import { useEffect, useRef } from "react";
import { gsap } from "gsap";

import { Header, Footer } from "..";

const PageWrapper = (Component, className, noFooter = false) => {
  return (compProps) => {
    const container = useRef();

    useEffect(() => {
      gsap.set(container.current, {
        opacity: 0,
      });
      if (compProps.intro) {
        gsap.to(container.current, {
          opacity: 1,
          duration: 0.8,
          ease: "power1.out",
        });
      }
    }, [compProps.intro]);

    return (
      <div className={className}>
        <Header />
        <div ref={container} style={{ opacity: 0 }}>
          <Component {...compProps} intro={compProps.intro} />
        </div>
        {noFooter ? null : <Footer />}
      </div>
    );
  };
};

export { PageWrapper };
