import cn from "classnames";

import s from "./ContactUsModal.module.css";
import { ModalWrapper, Select } from "../../commons";
import { Form } from "../../elements";
import { FAKE_CONST } from "../../../constants";

const ContactUsModal = ({ order }) => {
  const { selectFormOpations } = FAKE_CONST;

  return (
    <ModalWrapper containerClassName={cn({ [s.modal]: order })}>
      <div
        className={cn(s.container, "container", {
          [s.paddingRight]: order,
        })}
      >
        <div className={s.scroller}>
          {order ? null : (
            <>
              <span className={cn(s.selectTitle, "capitalizeText")}>
                category
              </span>
              <Select
                className={s.select}
                menuClassName={cn(s.selectMenu, "backdropFilter")}
                options={selectFormOpations}
                wrapperClassName={s.selectWrapper}
                placeholder={selectFormOpations[0].value}
              />
            </>
          )}
          <Form />
        </div>
        {order ? null : (
          <>
            <img className={s.img} src="/media/dynamic/bridalBg1.png" />
            <div className={s.backgroundImg}>
              <img src="/media/dynamic/bridalBg1.png" />
            </div>
          </>
        )}
      </div>
    </ModalWrapper>
  );
};

export { ContactUsModal };
