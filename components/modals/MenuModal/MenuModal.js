import { ModalWrapper } from "../../commons";
import { Menu } from "../../elements";

const MenuModal = () => {
  return (
    <ModalWrapper closeButtonType="2">
      <Menu />
    </ModalWrapper>
  );
};

export { MenuModal };
