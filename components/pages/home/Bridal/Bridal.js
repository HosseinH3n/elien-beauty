import { useEffect } from "react";
import cn from "classnames";
import gsap from "gsap";

import s from "./Bridal.module.css";
import { Border, ContextItems } from "../../../elements";
import { HOME_CONST } from "../../../../constants";

const Bridal = () => {
  const { desc, mainImage, parallaxImages } = HOME_CONST.bridal;

  useEffect(() => {
    const tl = gsap.timeline({
      ease: "power0.inOut",
      scrollTrigger: {
        trigger: `.${s.container}`,
        start: "top 75%",
        end: "bottom 15%",
        scrub: true,
      },
    });
    tl.to(
      `.${s.floatImg}`,
      {
        y: (n) => (n % 2 === 0 ? 1 : -1) * (90 + Math.random() * 80),
      },
      0
    );
    tl.to(
      `.${s.imgContent} img`,
      {
        scale: 0.95,
        xPercent: 6,
      },
      0
    );
  }, []);

  const _renderFloatImgs = (...slice) =>
    parallaxImages
      .slice(...slice)
      .map(({ id, img, ...style }) => (
        <img
          key={id}
          src={img}
          style={style}
          className={cn(s.floatImg, [s[`floatImg_${id}`]])}
        />
      ));

  return (
    <section className={cn(s.container)}>
      {_renderFloatImgs(0, 1)}
      <div className={cn(s.wrapper, "container", "backdropFilter")}>
        <Border />
        <div className={s.imgContent}>
          <img src={mainImage} alt="model" />
        </div>
        <div className={s.context}>
          <h2 className={cn(s.title, "capitalizeText")}>Bridal</h2>
          <p className={s.description}>{desc}</p>
          <ContextItems href="/p/bridal" />
          {_renderFloatImgs(1, 3)}
        </div>
        {_renderFloatImgs(3)}
      </div>
    </section>
  );
};

export { Bridal };
