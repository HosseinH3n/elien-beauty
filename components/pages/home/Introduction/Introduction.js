import { useEffect } from "react";
import cn from "classnames";
import gsap from "gsap";

import s from "./Introduction.module.css";
import { HOME_CONST } from "../../../../constants";
import { Icon } from "../../../commons";

const Introduction = ({ intro }) => {
  const { parallaxImages, desc, title } = HOME_CONST.introduction;

  useEffect(() => {
    gsap.set(`.${s.title} p`, {
      yPercent: 100,
      skewY: -20,
    });
    gsap.set(`.${s.description}`, {
      opacity: 0,
      yPercent: 5,
    });

    gsap.to(`.${s.floatImg}`, {
      y: (n) => (n % 2 === 0 ? -1 : 1) * (50 + Math.random() * 70),
      ease: "power0.inOut",
      scrollTrigger: {
        trigger: `.${s.container}`,
        start: "-100",
        end: "bottom top",
        scrub: true,
      },
    });
  }, []);

  useEffect(() => {
    gsap.set(`.${s.floatImg}`, {
      opacity: 0,
      y: (n) => (n % 2 === 0 ? 1 : -1) * (40 + Math.random() * 40),
    });
    if (intro) {
      const tl = gsap.timeline();
      tl.to(`.${s.title} p`, {
        duration: 1.5,
        stagger: 0.4,
        ease: "power2.out",
        yPercent: 0,
        skewY: 0,
        onComplete: () => {
          gsap.set(`.${s.title} span`, {
            css: { overflow: "visible", textShadow: "var(--text-shadow-1)" },
          });
        },
      });
      tl.to(
        `.${s.description}`,
        {
          opacity: 1,
          yPercent: 0,
          ease: "power1.out",
          duration: 0.7,
        },
        "-=0.7"
      );
      tl.to(
        `.${s.floatImg}`,
        {
          opacity: 1,
          ease: "power1.out",
          duration: 0.9,
          y: 0,
          stagger: 0.1,
        },
        "-=1"
      );
    }
  }, [intro]);

  return (
    <section className={s.container}>
      <div className="container">
        <h2 className={s.title}>
          <span>
            <p>Be your own kind</p>
          </span>
          <span>
            <p>of Beautiful</p>
          </span>
        </h2>
        <p className={s.description}>{desc}</p>
        <Icon icon="mouse" className={s.mouseIcon} />
      </div>
      {parallaxImages.map(({ id, img, ...style }) => (
        <img
          key={id}
          src={img}
          style={style}
          className={cn(s.floatImg, [s[`floatImg_${id}`]])}
        />
      ))}
    </section>
  );
};

export { Introduction };
