import { useEffect } from "react";
import cn from "classnames";
import gsap from "gsap";

import s from "./Lashes.module.css";
import { Border, ContextItems } from "../../../elements";
import { HOME_CONST } from "../../../../constants";

const Lashes = () => {
  const { contextItems, desc, mainImage, parallaxImages } = HOME_CONST.lashes;

  useEffect(() => {
    gsap.set(`.${s.backgroundText}`, { xPercent: -10 });
    const tl = gsap.timeline({
      ease: "power0.inOut",
      scrollTrigger: {
        trigger: `.${s.container}`,
        start: "top 75%",
        end: "bottom 15%",
        scrub: true,
      },
    });
    tl.to(
      `.${s.floatImg}`,
      {
        y: (n) => (n % 2 === 0 ? -1 : 1) * (90 + Math.random() * 70),
      },
      0
    );
    tl.to(
      `.${s.imgContent} img`,
      {
        scale: 0.95,
        xPercent: -6,
      },
      0
    );
    tl.to(
      `.${s.backgroundText}`,
      {
        xPercent: 0,
      },
      0
    );
  }, []);

  const _renderFloatImgs = (...slice) =>
    parallaxImages
      .slice(...slice)
      .map(({ id, img, ...style }) => (
        <img
          key={id}
          src={img}
          style={style}
          className={cn(s.floatImg, [s[`floatImg_${id}`]])}
        />
      ));

  return (
    <section className={s.container}>
      {_renderFloatImgs(0, 1)}
      <div className={cn(s.wrapper, "container")}>
        <span className={s.backgroundText}>Lashes & Brows</span>
        <div className={cn(s.context, "backdropFilter")}>
          <Border />
          <h2 className={cn(s.title, "capitalizeText")}>lashes & brows</h2>
          <p className={s.description}>{desc}</p>
          <ContextItems
            items={contextItems}
            itemClassName={s.contextItem}
            className={s.contextItemsWrapper}
          />
          {_renderFloatImgs(1)}
        </div>
        <div className={s.imgContent}>
          <img src={mainImage} alt="model" />
        </div>
      </div>
    </section>
  );
};

export { Lashes };
