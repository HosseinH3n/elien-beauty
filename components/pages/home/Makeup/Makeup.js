import { useEffect } from "react";
import cn from "classnames";
import gsap from "gsap";

import s from "./Makeup.module.css";
import { Border, ContextItems } from "../../../elements";
import { HOME_CONST } from "../../../../constants";

const Makeup = () => {
  const { contextItems, parallaxImages, desc, mainImage, resImage } =
    HOME_CONST.makeup;

  useEffect(() => {
    gsap.set(`.${s.backgroundText}`, { xPercent: -10 });
    const tl = gsap.timeline({
      ease: "power0.inOut",
      scrollTrigger: {
        trigger: `.${s.container}`,
        start: "top 75%",
        end: "bottom 15%",
        scrub: true,
      },
    });
    tl.to(
      `.${s.floatImg}`,
      {
        y: (n) => (n % 2 === 0 ? -1 : 1) * (90 + Math.random() * 80),
      },
      0
    );
    tl.to(
      `.${s.backgroundText}`,
      {
        xPercent: 0,
      },
      0
    );
    tl.to(
      `.${s.imgContent}`,
      {
        scale: 1.1,
      },
      0
    );
  }, []);

  const _renderFloatImgs = (...slice) =>
    parallaxImages
      .slice(...slice)
      .map(({ id, img, ...style }) => (
        <img
          key={id}
          src={img}
          style={style}
          className={cn(s.floatImg, [s[`floatImg_${id}`]])}
        />
      ));

  return (
    <section className={cn(s.container)}>
      {_renderFloatImgs(0, 1)}
      <div className={cn(s.wrapper, "container", "backdropFilter")}>
        <span className={s.backgroundText}>Makeup & Hairstyling</span>
        <Border />
        <div className={s.context}>
          <h2 className={cn(s.title, "capitalizeText")}>
            Makeup & Hairstyling
          </h2>
          <p className={s.description}>{desc}</p>
          <ContextItems
            items={contextItems}
            className={s.contextItemsWrapper}
            itemClassName={s.contextItem}
          />
          {_renderFloatImgs(1, 3)}
        </div>
        <div className={s.imgContent}>
          <picture>
            <source srcSet={resImage} media="(max-width: 990px)" />
            <img src={mainImage} alt="model" />
          </picture>
        </div>
        {_renderFloatImgs(3)}
      </div>
    </section>
  );
};

export { Makeup };
