import { useEffect } from "react";
import cn from "classnames";
import gsap from "gsap";

import s from "./Workshops.module.css";
import { Border, ContextItems } from "../../../elements";
import { HOME_CONST } from "../../../../constants";

const Workshops = () => {
  const { contextItems, parallaxImages, desc } = HOME_CONST.workshops;

  useEffect(() => {
    gsap.set(`.${s.backgroundText}`, { xPercent: -10 });
    const tl = gsap.timeline({
      ease: "power0.inOut",
      scrollTrigger: {
        trigger: `.${s.container}`,
        start: "top 75%",
        end: "bottom top",
        scrub: true,
      },
    });
    tl.to(
      `.${s.floatImg}:not(.${s.static})`,
      {
        y: () => 90 + Math.random() * 70,
      },
      0
    );
    tl.to(
      `.${s.backgroundText}`,
      {
        xPercent: 0,
      },
      0
    );
  }, []);

  return (
    <section className={cn(s.container, "container", "backdropFilter")}>
      <span className={s.backgroundText}>Workshops</span>
      <Border />
      <div className={s.context}>
        <h2 className={cn(s.title, "capitalizeText")}>Workshops</h2>
        <p className={s.description}>{desc}</p>
        <ContextItems
          items={contextItems}
          className={s.contextItemsWrapper}
          itemClassName={s.contextItem}
        />
      </div>
      {parallaxImages.map(({ id, img, isStatic, ...style }) => (
        <img
          key={id}
          src={img}
          style={style}
          className={cn(
            s.floatImg,
            {
              [s.static]: isStatic,
            },
            [s[`floatImg_${id}`]]
          )}
        />
      ))}
    </section>
  );
};

export { Workshops };
