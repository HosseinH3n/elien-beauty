const FAKE_CONST = {
  makeup: {
    title: "makeup",
    description: `Wil je schitteren voor een avondje uit of ga je naar een feestje of festival? Een aangepaste make-up en aangepast kapsel zullen ervoor zorgen dat je nog meer straalt.
 
		Hierbij hou ik uiteraard rekening met jouw persoonlijkheid, gelaatsvorm en outfit. Ik laat je stralen en zet je positieve punten nog meer in de kijker. ​Extra feestelijk of juist heel naturel? Alles is mogelijk! Ik werk met de professionele producten van Make-up Designory (MUD).`,
  },
  lash: {
    title: "lash volume lift",
    description: `Mooie lange en volle wimpers zorgen voor sprekende ogen en extra uitstraling. 

		Lash volume lift is een behandeling die wimpers langer, volumineuzer maakt en lift. Het zorgt voor een natuurlijke uitstraling en vraagt nauwelijks onderhoud.
		
		Aan het einde van de lash lifting worden je wimpers geverfd zodat het gebruik van mascara overbodig is. `,
  },
  hairstyling: {
    title: "hairstyling",
    description: `Ben je op zoek naar een geschikte hairstyling voor een speciale gelegenheid? Je kan bij mij terecht voor opsteekkapsels, vlechten, glamoureuze krullen krullen en andere feestelijke kapsels.`,
  },
  brows: {
    title: "HD Brows Original",
    description: `Bij HD Brows wordt de focus gelegd op bestaande haartjes voor een natuurlijk resultaat. Aan de hand van een op maat gemaakte kleuring en verschillende epilatietechnieken creëren we jouw ideale wenkbrauwvorm.`,
  },
  "workshop-makeup": {
    title: "Workshops Make-up",
    description: `Altijd al willen weten hoe op een relatief korte tijd een dagmake-up creëert?  Of mag het ook dat tikkeltje meer zijn en ga je voor een glamoureuze feestlook? Leer alle tips en tricks, probeer de producten uit en geniet van een hapje en een drankje.
    Het is ook een ideale gelegenheid om kennis te maken met de professionele producten van Make-up Designory (MUD). Na de workshop kan je, geheel vrijblijvend, deze producten aankopen.`,
  },
  "workshop-facial-care": {
    title: "Workshop gelaatsverzorging",
    description: `Maak op een leuke manier kennis met de producten voor gelaat- en lichaamsverzorging van Mila d’Opiz. Krijg een leerrijke demonstratie, probeer de producten uit en geniet van een hapje en een drankje. 

		Tijdens deze workshop krijg je antwoord op al je vragen omtrent de huid en leer ik jou stap voor stap hoe je je huid kan verzorgen.`,
  },
  communie: {
    title: "Communie of lentefeest",
    description: `Communie of lentefeest in aantocht? Uiteraard schitteren alle meisjes op deze grote dag. Een prachtig kapsel maakt het volledig af en transformeert hen tot dé prinses van de dag!

		Graag opgestoken haren, losse haren met krullen, een half opgestoken kapsel, vlechten, bloemen in het haar of een leuke haarband, diadeem of haarspelden? Dit is allemaal mogelijk.`,
  },
  bridal: {
    title: "bridal",
    description: `Eerst en vooral: gefeliciteerd met jullie verloving!
		Op jouw grote dag wil je vanzelfsprekend stralen. Ik creëer een make-up en haarlook die perfect bij jou past. 
		Enkele weken voor je trouw plannen we een try-out moment. Tijdens deze proefsessie bespreken we al jouw wensen, met jouw jurk, bruidsboeket, juwelen, enz. in het achterhoofd. 
		Op je trouwdag kom ik bij jou aan huis. Zo verlies je geen tijd en kan je je rustig klaarmaken`,
  },
  pagesServices: [
    {
      id: 1,
      title: "Full make-up",
      price: "45",
    },
    {
      id: 2,
      title: "Wimperplukjes",
      price: "5",
    },
    {
      id: 3,
      title: "Valse wimpers",
      price: "5",
    },
    {
      id: 4,
      title: "Combo make-up + hair(opsteekkapsel)",
      price: "80",
    },
  ],
  pagesGallery: [
    {
      id: 1,
      img: "/media/dynamic/makeupBg2.png",
    },
    {
      id: 2,
      img: "/media/dynamic/makeupBg1.png",
    },
    {
      id: 3,
      img: "/media/dynamic/makeupBg2.png",
    },
    {
      id: 4,
      img: "/media/dynamic/makeupBg2.png",
    },
    {
      id: 5,
      img: "/media/dynamic/makeupBg2.png",
    },
  ],
  selectFormOpations: [
    {
      value: "Makeup",
      label: "Makeup",
    },
    {
      value: "Lash volume lift",
      label: "Lash volume lift",
    },
  ],
};

export { FAKE_CONST };
