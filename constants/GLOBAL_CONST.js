import { Reoverlay } from "reoverlay";

const GLOBAL_CONST = {
  formInputs: [
    {
      label: "Voornaam",
      placeholder: "Vul uw voornaam in",
    },
    {
      label: "Achternaam",
      placeholder: "Vul je achternaam in",
    },
    {
      label: "E-mailadres",
      placeholder: "Vul je laatste e-mailadres in",
    },
    {
      type: "number",
      label: "Telefoon",
      placeholder: "Vul je laatste telefoon in",
    },
    {
      type: "textarea",
      label: "Beschrijving",
      placeholder: "Voer je beschrijving in",
    },
  ],
  footer: {
    description:
      "Wil je schitteren voor een avondje uit of ga je naar een feestje of festival? Een aangepaste make-up en aangepast kapsel zullen ervoor zorgen dat je nog meer straalt.",
    copyright: "Copyright © 2021 Elien Smekens. All rights reserved.",
    links: [
      {
        id: 1,
        title: "Menu",
        child: [
          {
            id: 1,
            title: "Makeup & Hairstyling",
            href: "/p/makeup",
            type: "link",
          },
          {
            id: 2,
            title: "Bridal",
            href: "/p/bridal",
            type: "link",
          },
          {
            id: 3,
            title: "Lashes & brows",
            href: "/p/lash",
            type: "link",
          },
          {
            id: 4,
            title: "Workshops",
            href: "/p/workshop-makeup",
            type: "link",
          },
        ],
      },
      {
        id: 2,
        title: "Contact Us",
        child: [
          {
            id: 1,
            icon: "calling",
            title: "+32-464-843246",
            href: "tel:+32464843246",
            type: "a",
          },
          {
            id: 2,
            icon: "message",
            title: "info@eliensmekens.be",
            href: "mailto:info@eliensmekens.be",
            type: "a",
          },
          {
            id: 3,
            icon: "location",
            title: "Iran, Tehran, Hafez St, No.14",
          },
        ],
      },
      {
        id: 3,
        title: "Get in touch",
        child: [
          {
            id: 1,
            title: "Questions or feedback?",
            onClick: () => Reoverlay.showModal("ContactUsModal"),
          },
        ],
        socialMedia: {
          title: "we'd love to hear from you",
          items: [
            {
              id: 1,
              href: "/",
              icon: "linkedin",
            },
            {
              id: 2,
              href: "/",
              icon: "facebook",
            },
            {
              id: 3,
              href: "/",
              icon: "instagram",
            },
            {
              id: 4,
              href: "/",
              icon: "twiiter",
            },
            {
              id: 5,
              href: "/",
              icon: "telegram",
            },
          ],
        },
      },
    ],
  },
};

export { GLOBAL_CONST };
