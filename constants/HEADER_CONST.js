const HEADER_CONST = {
  links: [
    {
      id: 1,
      href: "/",
      title: "Home",
    },
    {
      id: 2,
      href: "/p/makeup",
      title: "Makeup & Hairstyling",
    },
    {
      id: 3,
      href: "/p/bridal",
      title: "Bridal",
    },
    {
      id: 4,
      href: "/p/lash",
      title: "Lashes & brows",
    },
    {
      id: 5,
      href: "/p/workshop-makeup",
      title: "Workshops",
    },
    {
      id: 7,
      href: "/contacteer",
      title: "Contacteer ons",
    },
    {
      id: 8,
      href: "/privacy",
      title: "Privacy",
    },
  ],
  contactItems: [
    {
      id: 1,
      icon: "message",
      caption: "Email Adres",
      value: "info@eliensmekens.be",
    },
    {
      id: 2,
      icon: "calling",
      caption: "Telefoon",
      value: "+32-464-843246",
    },
    {
      id: 3,
      icon: "btw",
      caption: "BTW",
      value: "BE 0704.753.104",
    },
  ],
  socialMedia: [
    {
      id: 1,
      href: "/",
      icon: "facebook",
    },
    {
      id: 2,
      href: "/",
      icon: "instagram",
    },
  ],
};

export { HEADER_CONST };
