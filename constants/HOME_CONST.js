const HOME_CONST = {
  introduction: {
    parallaxImages: [
      {
        id: 1,
        "--top": "13px",
        "--left": "calc(50% - 43.6rem)",
        img: "/media/static/1.png",
      },
      {
        id: 2,
        "--top": "-25px",
        "--left": "calc(50% - 41.2rem)",
        img: "/media/static/2.png",
      },
      {
        id: 3,
        "--top": "57px",
        "--left": "calc(50% + 19.4rem)",
        img: "/media/static/3.png",
      },
      {
        id: 4,
        "--top": "388px",
        "--left": "0",
        img: "/media/static/4.png",
      },
      {
        id: 5,
        "--top": "396px",
        "--left": "calc(50% - 45rem)",
        img: "/media/static/5.png",
      },
      {
        id: 6,
        "--top": "131px",
        "--right": "0",
        img: "/media/static/6.png",
      },
      {
        id: 7,
        "--top": "283px",
        "--left": "calc(50% + 19.4rem)",
        img: "/media/static/7.png",
      },
    ],
    title: "Be your own kind of Beautiful",
    desc: "Welkom op mijn website! Ik ben Elien Smekens, make-upartist en hairstylist in de regio Oost-Vlaanderen. Verder ben ik ook schoonheidsspecialiste. Je kan bij mij terecht voor verschillende behandelingen: gelaatsverzorging, lashes & brows, manicure, pedicure en ontharingen. Al deze behandelingen zijn ook mogelijk aan huis",
  },
  makeup: {
    desc: "Wil je schitteren voor een avondje uit of ga je naar een feestje of festival? Een aangepaste make-up en aangepast kapsel zullen ervoor zorgen dat je nog meer straalt.",
    mainImage: "/media/dynamic/makeupBg1.png",
    resImage: "/media/dynamic/makeupBg2.png",
    parallaxImages: [
      {
        id: 1,
        zIndex: -2,
        "--top": "-17px",
        "--left": "-0.2rem",
        img: "/media/static/8.png",
      },
      {
        id: 2,
        "--top": "50px",
        "--left": "53.5%",
        img: "/media/static/9.png",
      },
      {
        id: 3,
        "--top": "58.6px",
        "--left": "71.5%",
        img: "/media/static/10.png",
      },
      {
        id: 4,
        zIndex: 200,
        "--bottom": "-18%",
        "--left": "-5.5%",
        img: "/media/static/11.png",
      },
    ],
    contextItems: [
      {
        id: 1,
        title: "Make-up",
        href: "/p/makeup",
      },
      {
        id: 2,
        title: "Communie of lentefeest",
        href: "/p/communie",
      },
      {
        id: 3,
        title: "Hairstyling",
        href: "/p/hairstyling",
      },
    ],
  },
  bridal: {
    desc: "Ga je binnenkort trouwen? Ik zorg ervoor dat je straalt op jouw speciale dag!",
    mainImage: "/media/dynamic/bridalBg1.png",
    parallaxImages: [
      {
        id: 1,
        zIndex: -1,
        "--top": "-82.9px",
        "--left": "0",
        img: "/media/static/12.png",
      },
      {
        id: 2,
        "--top": "calc(50% - 15rem)",
        "--left": "calc(50% - 19rem)",
        img: "/media/static/13.png",
      },
      {
        id: 3,
        "--top": "calc(50% - 12.8rem)",
        "--right": "calc(50% - 22.7rem)",
        img: "/media/static/14.png",
      },
      {
        id: 4,
        zIndex: 200,
        "--bottom": "-16%",
        "--right": "-1.4%",
        img: "/media/static/15.png",
      },
    ],
  },
  lashes: {
    desc: "Altijd al gedroomd van een stralende blik en langer lijkende wimpers? Of wil je graag mooi in vorm gebrachte en gekleurde wenkbrauwen of verkies je eerder een vollere vorm of fluffy brows?",
    mainImage: "/media/dynamic/lashesBg1.png",
    parallaxImages: [
      {
        id: 1,
        zIndex: -1,
        "--top": "-65px",
        "--left": "0",
        img: "/media/static/16.png",
      },
      {
        id: 2,
        "--top": "42.9px",
        "--left": "54.6%",
        img: "/media/static/17.png",
      },
      {
        id: 3,
        "--top": "216.6px",
        "--left": "3.5%",
        img: "/media/static/18.png",
      },
      {
        id: 4,
        zIndex: 200,
        "--bottom": "-7.5%",
        "--right": "1%",
        img: "/media/static/19.png",
      },
    ],
    contextItems: [
      {
        id: 1,
        title: "Lash volume lift",
        href: "/p/lash",
      },
      {
        id: 2,
        title: "HD Brows Original",
        href: "/p/brows",
      },
    ],
  },
  workshops: {
    desc: "Kent make-up nog veel geheimen voor jou? Weet je niet goed hoe je je huid nu best verzorgt? Krijg een leerrijke demonstratie, leer alle tips en tricks over make-up, probeer de producten uit en geniet van een hapje en een drankje.",
    parallaxImages: [
      {
        id: 1,
        zIndex: 200,
        "--top": "-35px",
        "--left": "-4.4%",
        img: "/media/static/2.png",
      },
      {
        id: 2,
        "--top": "48px",
        "--left": "18.3%",
        img: "/media/static/20.png",
      },
      {
        id: 3,
        "--top": "104px",
        "--left": "58.5%",
        img: "/media/static/21.png",
      },
      {
        id: 4,
        zIndex: 200,
        "--bottom": "0",
        "--right": "0",
        img: "/media/static/22.png",
        isStatic: true,
      },
      {
        id: 5,
        zIndex: 200,
        "--bottom": "0",
        "--right": "0",
        img: "/media/static/23.png",
        isStatic: true,
      },
    ],
    contextItems: [
      {
        id: 1,
        title: "Workshop facial care",
        href: "/p/workshop-facial-care",
      },
      {
        id: 2,
        title: "Workshop make-up",
        href: "/p/workshop-makeup",
      },
    ],
  },
};

export { HOME_CONST };
