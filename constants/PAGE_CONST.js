const PAGE_CONST = {
  pagesConfig: {
    hairstyling: {
      floatImgIndex: 2,
      floatImgStyle: 2,
    },
    brows: {
      floatImgIndex: 2,
      floatImgStyle: 3,
    },
    "workshop-facial-care": {
      floatImgIndex: 2,
      floatImgStyle: 3,
    },
    "workshop-makeup": {
      floatImgIndex: 1,
      floatImgStyle: 1,
    },
    makeup: {
      floatImgIndex: 1,
      floatImgStyle: 1,
    },
    lash: {
      floatImgIndex: 1,
      floatImgStyle: 1,
    },
    communie: {
      floatImgIndex: 3,
      floatImgStyle: 6,
    },
    bridal: {
      floatImgIndex: 4,
      floatImgStyle: 5,
    },
  },
  floatImages: {
    1: "/media/static/10.png",
    2: "/media/static/26.png",
    3: "/media/static/27.png",
    4: "/media/static/28.png",
    floatStyle_1: {
      left: "0.1em",
      bottom: "-0.73em",
    },
    floatStyle_2: {
      left: "2.75em",
      bottom: "-0.32em",
    },
    floatStyle_3: {
      left: "2em",
      bottom: "-0.05em",
    },
    floatStyle_4: {
      left: "0.73em",
      bottom: "-0.15em",
    },
    floatStyle_5: {
      left: "0.72em",
      bottom: "-0.75em",
    },
    floatStyle_6: {
      left: "3.52em",
      bottom: "0.05em",
    },
  },
  allPages: [
    {
      category: 1,
      title: "Make up",
      id: "makeup",
    },
    {
      category: 1,
      title: "Communie",
      id: "communie",
    },
    {
      category: 1,
      title: "Hairstyling",
      id: "hairstyling",
    },
    {
      category: 2,
      title: "Lash volume lift",
      id: "lash",
    },
    {
      category: 2,
      title: "HD Brows Original",
      id: "brows",
    },
    {
      category: 3,
      title: "Workshop facial care",
      id: "workshop-facial-care",
    },
    {
      category: 3,
      title: "Workshop make-up",
      id: "workshop-makeup",
    },
  ],
};

export { PAGE_CONST };
