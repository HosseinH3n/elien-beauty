import React, { useState, useEffect } from "react";

const SizeContext = React.createContext(0);

const WindowSizeProvider = ({ children }) => {
  const [size, setSize] = useState(0);

  useEffect(() => {
    onResize();
    window.addEventListener("resize", onResize);
    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  const onResize = () => {
    window && setSize(window.innerWidth);
  };

  return <SizeContext.Provider value={size}>{children}</SizeContext.Provider>;
};

export { WindowSizeProvider, SizeContext };
