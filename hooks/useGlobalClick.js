import { useEffect } from "react";

const useGlobalClick = (initialRef, ref, callBack) => {
  const handleClick = ({ target }) => {
    if (initialRef.current.contains(target)) return;
    if (ref.current && ref.current.contains(target) === false) {
      callBack();
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleClick);

    return () => {
      document.removeEventListener("click", handleClick);
    };
  });
};

export { useGlobalClick };
