import { useContext } from "react";

import { SizeContext } from "../context";

let userAgent = "";

const useMobile = (deviceSize) => {
  const size = useContext(SizeContext);

  return size
    ? size <= deviceSize
    : userAgent.match(/Android/i) ||
        userAgent.match(/webOS/i) ||
        userAgent.match(/iPhone/i) ||
        userAgent.match(/iPad/i) ||
        userAgent.match(/iPod/i) ||
        userAgent.match(/BlackBerry/i) ||
        userAgent.match(/Windows Phone/i);
};

const setIsMobile = (agent) => {
  if (!agent) return;
  userAgent = agent;
};

export { useMobile, setIsMobile };
