import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { Reoverlay, ModalContainer } from "reoverlay";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Scrollbar from "smooth-scrollbar";
import gsap from "gsap";
import { useRouter } from "next/router";
import { ToastContainer } from "react-toastify";

import "swiper/swiper.scss";
import "swiper/components/pagination/pagination.scss";
import "video-react/dist/video-react.css";
import "react-toastify/dist/ReactToastify.css";

import "../styles/global.css";
import { WindowSizeProvider } from "../context";
import { ContactUsModal, MenuModal } from "../components/modals";
import { Loading } from "../components/elements";

Reoverlay.config([
  {
    name: "ContactUsModal",
    component: ContactUsModal,
  },
  {
    name: "MenuModal",
    component: MenuModal,
  },
]);

const App = ({ Component, pageProps }) => {
  const [intro, setIntro] = useState(false);
  const bodyScrollbar = useRef();
  const router = useRouter();

  useLayoutEffect(() => {
    gsap.registerPlugin(ScrollTrigger);
    const scroller = document.querySelector("#viewport");
    bodyScrollbar.current = Scrollbar.init(scroller, {
      damping: 0.07,
      delegateTo: document,
      alwaysShowTracks: false,
    });
    const removeScroll = () => (scroller.style.overflow = "visible");
    ScrollTrigger.addEventListener("refresh", () => {
      removeScroll();
      requestAnimationFrame(removeScroll);
    });
    bodyScrollbar.current.addListener(ScrollTrigger.update);
    ScrollTrigger.defaults({ scroller });
    ScrollTrigger.scrollerProxy(scroller, {
      scrollTop(value) {
        if (arguments.length) {
          bodyScrollbar.current.scrollTop = value;
        }
        return bodyScrollbar.current.scrollTop;
      },
      getBoundingClientRect() {
        return {
          top: 0,
          left: 0,
          width: window.innerWidth,
          height: window.innerHeight,
        };
      },
    });
  }, []);

  useEffect(() => {
    const handleRouteChange = () => {
      scrollTo(0);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, []);

  const scrollTo = (y, plus = false) => {
    console.log("SASAS");
    bodyScrollbar.current.scrollTo(
      0,
      y + (plus ? bodyScrollbar.current.scrollTop : 0),
      800
    );
  };

  return (
    <WindowSizeProvider>
      <Loading onLoad={() => setIntro(true)} />
      <div id="viewport">
        <div className="app">
          <Component {...pageProps} scrollTo={scrollTo} intro={intro} />
        </div>
      </div>
      <ModalContainer />
      <ToastContainer
        closeOnClick
        pauseOnHover={false}
        draggable
        position="top-left"
        className="toast-component"
        autoClose={4000}
      />
    </WindowSizeProvider>
  );
};

export default App;
