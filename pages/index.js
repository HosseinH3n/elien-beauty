import { PageWrapper } from "../components/elements";
import {
  Introduction,
  Makeup,
  Bridal,
  Lashes,
  Workshops,
} from "../components/pages/home";

const Home = ({ intro }) => {
  return (
    <main>
      <Introduction intro={intro} />
      <Makeup />
      <Bridal />
      <Lashes />
      <Workshops />
    </main>
  );
};

export default PageWrapper(Home);
