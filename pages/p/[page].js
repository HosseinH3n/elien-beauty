import { Fragment, useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import cn from "classnames";
import gsap from "gsap";
import { Reoverlay } from "reoverlay";

import s from "./page.module.css";
import { Button, Carousel } from "../../components/commons";
import { Border, CurveStepper, PageWrapper } from "../../components/elements";
import { useMobile } from "../../hooks";
import { FAKE_CONST, PAGE_CONST } from "../../constants";

const { allPages, pagesConfig } = PAGE_CONST;

const Page = ({ intro }) => {
  const [activeImg, setActiveImg] = useState(0);
  const isMediumSize = useMobile(790);
  const {
    query: { page },
    push,
  } = useRouter();
  const tl = useRef(gsap.timeline({ paused: true }));
  const { category } = allPages.find((i) => i.id === page) || {};
  const gallery = FAKE_CONST.pagesGallery;
  const steps = category ? allPages.filter((i) => i.category === category) : [];
  const { floatImgIndex, floatImgStyle } = pagesConfig[page] || {};
  const { title = "", description = "" } = FAKE_CONST[page] || {};

  const _renderGalleryImgs = ({ img }, index) => (
    <img
      src={img}
      key={index}
      alt="model"
      className={cn(s.image, {
        [s.active]: !isMediumSize && index === activeImg,
      })}
      onClick={() => {
        if (index === activeImg) return;
        gsap.to(`.${s.backgroundImg}`, {
          opacity: 0,
          duration: 0.4,
          ease: "power1.in",
          onComplete: () => {
            setActiveImg(index);
            gsap.to(`.${s.backgroundImg}`, {
              opacity: 1,
              duration: 0.4,
              ease: "power0.out",
            });
          },
        });
      }}
    />
  );

  useEffect(() => {
    tl.current.to(`.${s.description}, .${s.titleWrapper}`, {
      opacity: 1,
      y: 0,
      duration: 0.5,
      stagger: {
        each: 0.1,
        from: "end",
      },
      ease: "power1.out",
    });
    tl.current.to(
      `.${s.gallery}, .${s.backgroundImg}`,
      {
        opacity: 1,
        duration: 0.5,
        ease: "power1.out",
      },
      "-=0.5"
    );
    tl.current.to(
      `.${s.footer}`,
      {
        yPercent: 0,
        duration: 0.5,
        ease: "power1.out",
      },
      "-=0.5"
    );
  }, []);

  useEffect(() => {
    if (intro) {
      gsap.set(`.${s.titleWrapper}, .${s.description}`, {
        opacity: 0,
        y: -30,
      });
      gsap.set(`.${s.gallery}, .${s.backgroundImg}`, {
        opacity: 0,
      });
      gsap.set(`.${s.footer}`, {
        yPercent: 100,
      });
      tl.current.play();
    }
  }, [intro]);

  const handleChangePage = (item) => {
    tl.current.reverse();
    tl.current.eventCallback("onReverseComplete", () => {
      setActiveImg(0);
      push("/p/" + item.id);
      tl.current.play();
    });
  };

  return (
    <Fragment>
      <main className={cn(s.container, "container")}>
        <CurveStepper
          className={s.stepper}
          steps={steps}
          selected={page}
          onSelect={handleChangePage}
        />
        <div>
          <img src={gallery[activeImg].img} className={s.backgroundImg} />
          <div className={s.titleWrapper}>
            <img
              src={PAGE_CONST.floatImages[floatImgIndex]}
              className={s.backgroundTitle}
              style={PAGE_CONST.floatImages[`floatStyle_${floatImgStyle}`]}
            />
            <h1 className={cn(s.title, "capitalizeFirstLetterText")}>
              {title}
            </h1>
          </div>
          <p className={s.description}>{description}</p>
          <div className={s.gallery}>
            <div className="capitalizeFirstLetterText">
              {title}
              <span className="capitalizeText"> gallery </span>
            </div>
            {isMediumSize ? (
              <Carousel
                multi
                hasDynamicSlide
                cardNumber="auto"
                spaceBetween={7}
                slides={gallery}
                className={s.carousel}
                swiperClassName={s.swiper}
                renderSlide={_renderGalleryImgs}
              />
            ) : (
              <div className={s.imageWrapper}>
                {gallery.map(_renderGalleryImgs)}
              </div>
            )}
          </div>
        </div>
        <footer className={s.footer}>
          <ul className={s.services}>
            {FAKE_CONST.pagesServices.map(({ id, title, price }) => (
              <li key={id} className={s.servicesItem}>
                <span>{title}</span>
                <span className={s.unit}>
                  €<span className={s.price}>{price}</span>
                </span>
              </li>
            ))}
          </ul>
          <Button
            reverse
            primary
            theme="2"
            onClick={() => {
              Reoverlay.showModal("ContactUsModal", { order: page });
            }}
            title="adspraak"
            icon="arrow-right"
            prefix={<Border />}
            className={cn(s.transfer, "backdropFilter", "upperCaseText")}
          />
        </footer>
      </main>
    </Fragment>
  );
};

export async function getServerSideProps() {
  return {
    props: {},
  };
}

export default PageWrapper(Page, null, true);
