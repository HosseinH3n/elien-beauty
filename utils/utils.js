import { toast } from "react-toastify";

class Utils {
  isStandalone = () => {
    return window.matchMedia("(display-mode: standalone)").matches;
  };
  putCommas = (number) => {
    if (!number) return number;
    if (typeof number === "undefined") return number;
    if (typeof number === "number") number = number.toString();
    return number.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  };
  createRange = (start, end) => {
    return Array(end - start + 1)
      .fill()
      .map((_, index) => start + index);
  };
  toEnglishDigits = (str) => {
    // convert persian digits [۰۱۲۳۴۵۶۷۸۹]
    var e = "۰".charCodeAt(0);
    str = str.replace(/[۰-۹]/g, function (t) {
      return t.charCodeAt(0) - e;
    });
    // convert arabic indic digits [٠١٢٣٤٥٦٧٨٩]
    e = "٠".charCodeAt(0);
    str = str.replace(/[٠-٩]/g, function (t) {
      return t.charCodeAt(0) - e;
    });
    return str;
  };
  hasNumber = (term) => {
    return /\d/.test(term);
  };
  parseDigit(str = "") {
    var faNum = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    return String(str).replace(/[0-9]/g, function (w) {
      return faNum[+w];
    });
  }
  toPersianDate = (date) => {
    return date
      .toLocaleDateString("fa-IR", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
      })
      .split(",")
      .join("")
      .split(" ")
      .reverse()
      .join(" ");
  };
  lockScroll = () => {
    // document.querySelector(".app").style.overflowY = "clip";
  };
  enableScroll = () => {
    // document.querySelector(".app").style.overflowY = "initial";
  };
  preventDefault(e) {
    e.preventDefault();
  }
  chunkArray(arr, chunk_size) {
    let result = [];
    for (let index = 0; index < arr.length; index += chunk_size) {
      const myChunk = arr.slice(index, index + chunk_size);
      result.push(myChunk);
    }
    return result;
  }
  etcString(str = "", count = 10) {
    return str && str !== null
      ? str.length < count
        ? str
        : str.slice(0, count - 2) + "..."
      : null;
  }
  isServer() {
    return !(typeof window != "undefined" && window.document);
  }
  cloneObj(obj) {
    return JSON.parse(JSON.stringify(obj));
  }
  showAlert(str = "", type = "error", options = {}) {
    return toast(str, {
      type,
      ...options,
    });
  }
  needLogin(cb = () => false) {
    if ("guest") this.showAlert("ابتدا باید وارد  شوید", "info");
    else cb();
  }
}

const utils = new Utils();
export default utils;
